#!/bin/bash -xe

PARAMS_FILE=$1

SCRIPTS_HOME="@PIPELINE_DIR_PATH@/scripts"

SETUP="$SCRIPTS_HOME/freesurfer60_setup.sh"

# source params we need from bash params file.
source $PARAMS_FILE || die "Could not source params file $PARAMS_FILE"

if [[ -z $recon_all_args ]]; then
    die "Could not launch Freesurfer. No arguments received for recon-all."
fi

# Construct recon-all args from params
RECON_ALL_ARGS="-s $label -sd $freesurferdir $recon_all_args"

# If we are relaunching FS, we are done. If not, we need to figure out our input files.
if [[ $relaunch != true ]]; then

    for t1 in ${scan_ids[*]}; do
        scan_file=`ls ${rawdir}/$t1 | head -1`
        # if [[ -z $scan_file ]]; then
        #     die "Could not find a file in directory ${rawdir}/$t1"
        # fi
        RECON_ALL_ARGS="$RECON_ALL_ARGS -i ${rawdir}/${t1}/$scan_file"
    done

    for t2 in ${t2_ids[*]}; do
        scan_file=`ls ${rawdir}/$t2 | head -1`
        # if [[ -z $scan_file ]]; then
        #     die "Could not find a file in directory ${rawdir}/$t2"
        # fi
        RECON_ALL_ARGS="$RECON_ALL_ARGS -T2 ${rawdir}/${t2}/$scan_file"
    done

    # Add the T2pial flag if we have T2 IDs being included
    if [ "${t2_ids}" != "" ] && [ ${#t2_ids[@]} -gt 0 ]; then
        RECON_ALL_ARGS="$RECON_ALL_ARGS -T2pial"
    fi

    for flair in ${flair_ids[*]}; do
        scan_file=`ls ${rawdir}/$flair | head -1`
        # if [[ -z $scan_file ]]; then
        #     die "Could not find a file in directory ${rawdir}/$flair"
        # fi
        RECON_ALL_ARGS="$RECON_ALL_ARGS -FLAIR ${rawdir}/${flair}/$scan_file"
    done

    # Add the FLAIRpial flag if we have FLAIR IDs being included
    if [ "${flair_ids}" != "" ] && [ ${#flair_ids[@]} -gt 0 ]; then
        RECON_ALL_ARGS="$RECON_ALL_ARGS -FLAIRpial"
    fi

fi

# Set up Freesurfer
source $SETUP || die "Could not source setup script $SETUP"

$FREESURFER_HOME/bin/recon-all $RECON_ALL_ARGS || die "recon-all failed"
